import { Types } from '../constants/actionTypes';
import { removeItem } from '../utils';

export const ActionCreators = {

  login: (user) => ({ type: Types.LOGIN, payload: { user } }),

  logout: () => { 
    removeItem("username");
    removeItem("password");
    return({ type: Types.LOGOUT, payload: { email: '', username: '' } })
  }
  
}