import { Types } from '../constants/actionTypes';
import { REST_COUNTRIES_API } from '../constants/api';
import axios from 'axios'

export const ActionCreators = {

    getCountries: () => async dispatch => {
    
        try{
            const res = await axios.get(`${REST_COUNTRIES_API}rest/v2/all`)
            res.data.map(function(c,index){
                c.id = index;
                return c;
            })
            dispatch( {
                type: Types.GET_COUNTRIES,
                payload: res.data
            })
        }
        catch(e){
            dispatch( {
                type: Types.COUNTRIES_ERROR,
                payload: console.log(e),
            })
        }

    }
  
}