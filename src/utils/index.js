export const setStore = (name, content) => {
    if (!name) return
    return localStorage.setItem(name, content)
}

export const getStore = (name) => {
    if (!name) return
    return localStorage.getItem(name)
}

export const removeItem = (name) => {
    if (!name) return
    return localStorage.removeItem(name)
}