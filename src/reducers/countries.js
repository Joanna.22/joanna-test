import { Types } from '../constants/actionTypes';

const initialState = {
  countries: {
    data: [],
    fetched: false
  }
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case Types.GET_COUNTRIES:
        return {
            ...state,
            countries: {
                data: action.payload,
                fetched: true
            }
        }
    case Types.COUNTRIES_ERROR:
        return {
            ...state,
            countries: {
                data: action.payload,
                fetched: false
            }
        }
    default:
      return state;
  }
}

export default reducer;