import { Types } from '../constants/actionTypes';

const initialState = {
  profile: {
    email: '',
    username: ''
  }
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case Types.LOGIN:
      return {
        ...state,
        profile: action.payload.user
      }
    case Types.LOGOUT:
      return {
        ...state,
        profile: action.payload.user
      }
    default:
      return state;
  }
}

export default reducer;