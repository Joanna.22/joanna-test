import React, {useEffect} from 'react';
import { withRouter, Link } from "react-router-dom";
import { connect } from 'react-redux';
import { ActionCreators } from '../actions/countries';
import { DataGrid } from '@material-ui/data-grid';
import Visibility from '@material-ui/icons/Visibility';
import DragIndicator from '@material-ui/icons/DragIndicator';
import Loader from './layout/Loader';

function Home(props) {

    const columns = [
        { field: 'alpha2Code', headerName: 'Alpha2Code', headerClassName: 'countries-table-header' },
        { field: 'name', headerName: 'Name', headerClassName: 'countries-table-header' },
        { field: 'capital', headerName: 'Capital', headerClassName: 'countries-table-header' },
        { field: 'col4', headerName: 'Actions', headerClassName: 'countries-table-header', 
            renderCell: (params) => (<Link className="eye-container" to={`/details/${params.id}`}><Visibility style={{ color: '#006782' }} /></Link>),
            renderHeader: (params) => (<div className="MuiDataGrid-colCellTitle">Actions <DragIndicator style={{ color: '#7b7b7b' }} /></div>) },
    ];

    useEffect(()=>{
        props.dispatch(ActionCreators.getCountries());
    // eslint-disable-next-line react-hooks/exhaustive-deps
    },[]);

    return(
        <div className="countries-table hv-center">
            {
                props.fetched?
                    <DataGrid
                        columns={columns}
                        rows={props.countries}
                        pageSize={10}
                        pagination 
                    />
                :
                    <Loader/>
            }
        </div>
    )
}

const mapStateToProps = (state) => {
    return {
      countries: state.countries.countries.data,
      fetched: state.countries.countries.fetched
    }
}

export default connect(mapStateToProps)(withRouter(Home));