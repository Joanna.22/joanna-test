import React, {useState, useEffect} from 'react';
import { withRouter, useParams } from "react-router-dom";
import { connect } from 'react-redux';
import Place from '@material-ui/icons/Place';
import ConfirmationNumber from '@material-ui/icons/ConfirmationNumber';
import ZoomOutMap from '@material-ui/icons/ZoomOutMap';
import Language from '@material-ui/icons/Language';
import MonetizationOn from '@material-ui/icons/MonetizationOn';
import { ActionCreators } from '../actions/countries';
import Loader from './layout/Loader';

function Details(props) {
    const { countryID } = useParams();

    const [countryInfo, setCountryInfo] = useState({});

    useEffect(()=>{
        let countryList = props.countries;
        if(countryList.length === 0){
            props.dispatch(ActionCreators.getCountries());
        }
        let selectedCountry = countryList.filter(function(c){
            return c.id === parseInt(countryID)
        })[0]
        setCountryInfo(selectedCountry)
    },[countryID, props.countries]);
    if(!props.fetched){
        return(
            <div className="details-container">
                <Loader/>
            </div>
        )
    }else{
        return(
            <div className="details-container">
                <div className="row justify-content-between">
                    <img className="col-sm-5 box-shadow p-0 country-img" src={countryInfo?.flag} alt={countryInfo?.alpha2Code} />
                    <div className="col-sm-5 box-shadow p-5 main-info-container">
                        <ul className="d-flex flex-column justify-content-between mb-0 h-100">
                            <li className="main-info"><span className="bold-font">Name:</span> {countryInfo?.name}</li>
                            <li className="main-info"><span className="bold-font">Native name:</span> {countryInfo?.nativeName}</li>
                            <li className="main-info"><span className="bold-font">Capital:</span> {countryInfo?.capital}</li>
                        </ul>
                    </div>
                </div>
                <div className="row box-shadow mt-5 flex-row section-container">
                    <div className="col d-flex flex-row align-items-center pl-0">
                        <h1 className="h-100 d-flex align-items-center bold-font bg-greenblue mb-0 section-title">Geographic information <Place className="place-icon"/></h1>
                        <ul className="d-flex flex-column justify-content-center mb-0 h-100 section-info">
                            <li className="main-info"><span className="bold-font">Region:</span> {countryInfo?.region}</li>
                            <li className="main-info"><span className="bold-font">Sub-region:</span> {countryInfo?.subregion}</li>
                            <li className="main-info"><span className="bold-font">Timezone:</span> {countryInfo?.timezones? countryInfo?.timezones[0] : null}</li>
                        </ul>
                    </div>
                </div>
                <div className="row box-shadow mt-5 flex-row section-container">
                    <div className="col d-flex flex-row align-items-center pl-0">
                        <h1 className="h-100 d-flex align-items-center bold-font bg-greenblue mb-0 section-title">Codes<ConfirmationNumber className="code-icon"></ConfirmationNumber></h1>
                        <ul className="d-flex flex-column justify-content-center mb-0 h-100 section-info">
                            <li className="main-info"><span className="bold-font">Alpha 2-code:</span> {countryInfo?.alpha2Code}</li>
                            <li className="main-info"><span className="bold-font">Alpha 3-code:</span> {countryInfo?.alpha3Code}</li>
                            <li className="main-info"><span className="bold-font">Numeric code:</span> {countryInfo?.numericCode}</li>
                        </ul>
                    </div>
                </div>
                <div className="row box-shadow mt-5 flex-row section-container">
                    <div className="col d-flex flex-row align-items-center pl-0">
                        <h1 className="h-100 d-flex align-items-center bold-font bg-greenblue mb-0 section-title">Borders<ZoomOutMap className="border-icon"/></h1>
                        <ul className="d-flex flex-column justify-content-center mb-0 h-100 section-info">
                            {
                                countryInfo?.borders?.map(function(b, index){
                                    let liCountry = props.countries.filter(function(c){return c.alpha3Code === b})[0];
                                    return(
                                        <li key={index} className="main-info">
                                            <span className="bold-font">
                                                {liCountry?.name}
                                            </span>
                                            <img className="p-0 flag-icon" src={liCountry.flag} alt={liCountry.alpha2Code} />
                                        </li>
                                    )
                                })
                            }
                        </ul>
                    </div>
                </div>
                <div className="row box-shadow mt-5 flex-row section-container">
                    <div className="col d-flex flex-row align-items-center pl-0">
                        <h1 className="h-100 d-flex align-items-center bold-font bg-greenblue mb-0 section-title">Languages<Language className="lang-icon"/></h1>
                        <ul className="d-flex flex-column justify-content-center mb-0 h-100 section-info">
                            {
                                countryInfo?.languages?.map(function(l, index){
                                    return(
                                        <li key={index} className="main-info"><span className="bold-font">{l.name}</span> ({l.nativeName})</li>
                                    )
                                })
                            }
                        </ul>
                    </div>
                </div>
                <div className="row box-shadow mt-5 flex-row section-container">
                    <div className="col d-flex flex-row align-items-center pl-0">
                        <h1 className="h-100 d-flex align-items-center bold-font bg-greenblue mb-0 section-title">Currencies<MonetizationOn className="currency-icon"/></h1>
                        <ul className="d-flex flex-column justify-content-center mb-0 h-100 section-info">
                            {
                                countryInfo?.currencies?.map(function(c, index){
                                    return(
                                        <li key={index} className="main-info"><span className="bold-font">{c.code} ({c.symbol}):</span> {c.name}</li>
                                    )
                                })
                            }
                        </ul>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
      countries: state.countries.countries.data,
      fetched: state.countries.countries.fetched
    }
}

export default connect(mapStateToProps)(withRouter(Details));