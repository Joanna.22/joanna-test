import React, {useState, useEffect} from 'react';
import { withRouter } from "react-router-dom";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import { connect } from 'react-redux';
import { ActionCreators } from '../../actions/login';
import { setStore, getStore } from '../../utils';

function Login(props) {

    const LoginSchema = Yup.object().shape({
        email: Yup.string().email().required("Email is required"),
      
        username: Yup.string().required("Username is required"),

        password: Yup.string()
          .required("Password is required")
          .min(8, "Password is too short - should be 8 chars minimum"),
    });

    // eslint-disable-next-line no-unused-vars
    const [state , setState] = useState({
        email: "",
        username: "",
        password: ""
    })

    useEffect(()=>{
        const olduser = getStore("username");
        if(olduser){
            props.dispatch(ActionCreators.logout());
        }   
    // eslint-disable-next-line react-hooks/exhaustive-deps
    },[]);

    return(
        <div className="card login-card p-4 hv-center">
            <Formik
                initialValues={state}
                validationSchema={LoginSchema}
                onSubmit={async (values) => {
                    const user = values;
                    setStore("username",values.username);
                    setStore("password",btoa(values.password));
                    props.dispatch(ActionCreators.login(user));
                    props.history.push('/home');
                }}
                >
                {(formik) => {
                    const { errors, touched, isValid, dirty } = formik;
                    return (
                        <Form>
                            <div className="form-group mb-4 text-left">
                                <label className="bold-font" htmlFor="exampleInputEmail1">Email address</label>
                                <Field
                                    type="email"
                                    name="email"
                                    id="email"
                                    className={
                                        errors.email && touched.email ? "input-error form-control" : "form-control"
                                    }
                                />
                                <ErrorMessage name="email" component="span" className="error-msg" />
                            </div>
                            <div className="form-group mb-4 text-left">
                                <label className="bold-font" htmlFor="exampleInputEmail1">Username</label>
                                <Field
                                    name="username"
                                    id="username"
                                    className={
                                        errors.username && touched.username ? "input-error form-control" : "form-control"
                                    }
                                />
                                <ErrorMessage name="username" component="span" className="error-msg" />
                            </div>
                            <div className="form-group mb-4 text-left">
                                <label className="bold-font" htmlFor="exampleInputPassword1">Password</label>
                                <Field
                                    type="password"
                                    name="password"
                                    id="password"
                                    className={
                                        errors.password && touched.password ? "input-error form-control" : "form-control"
                                    }
                                />
                                <ErrorMessage
                                    name="password"
                                    component="span"
                                    className="error-msg"
                                />
                            </div>
                            <div className="form-check">
                            </div>
                            <button
                                type="submit"
                                className={!(dirty && isValid) ? "disabled-btn btn jm-btn" : "btn jm-btn"}
                                disabled={!(dirty && isValid)}
                            >
                                Sign In
                            </button>
                        </Form>
                    );
                }}
            </Formik>
        </div>
    )
}

const mapStateToProps = (state) => {
    return {
      profile: state.user.profile
    }
}

export default connect(mapStateToProps)(withRouter(Login));