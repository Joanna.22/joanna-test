import React from 'react';

export default function Loader() {
    return(
        <div className="loader">
            <div className="lds-default round">
                <div className="dots-circle-spinner" />
            </div>
        </div>
    )
}