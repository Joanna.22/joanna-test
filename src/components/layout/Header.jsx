import React, {useState, useEffect} from 'react';
import { withRouter, useHistory } from "react-router-dom";
import { connect } from 'react-redux';
import { getStore } from '../../utils';
import { ActionCreators } from '../../actions/login';
import KeyboardBackspace from '@material-ui/icons/KeyboardBackspace';
import AccountCircle from '@material-ui/icons/AccountCircle';

function Header(props) {
    let history = useHistory();
    const [usermame, setUsermame] = useState("");
    useEffect(()=>{
        let user = getStore('username');
        if(user){
            setUsermame(user)
        }else{
            setUsermame("")
        }
    },[props.profile]);
    return(
        <nav className="navbar navbar-dark bg-greenblue">
            {usermame !== ""?
                <div className="row col-12 d-flex justify-content-end text-white">
                    <button className="back-container" onClick={()=>{history.goBack()}}><KeyboardBackspace className="back-icon"/></button>
                    <div className="dropdown">
                        <button className="profile-btn dropdown-toggle" type="button" id="dropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <AccountCircle className="profile-icon"/><span className="h3 username-txt">{usermame}</span>
                        </button>
                        <div className="dropdown-menu box-shadow" aria-labelledby="dropdownProfile">
                            <a className="dropdown-item" href="#" onClick={()=>{props.dispatch(ActionCreators.logout())}}>Logout</a>
                        </div>
                    </div>
                </div>
            :
                <div className="row col-12 d-flex justify-content-center text-white">
                    <span className="h3 bold-font">Login</span>
                </div>
            }
        </nav>
    )
}

const mapStateToProps = (state) => {
    return {
        profile: state.user.profile
    }
}

export default connect(mapStateToProps)(withRouter(Header));