import './assets/scss/index.scss';
import Header from './components/layout/Header';
import Navigation from './navigation';
import { BrowserRouter as Router } from "react-router-dom";

function App() {
  return (
    <Router>
      <div className="App">
        <Header />
        <div className="app-container">
          <div className="app-content container justify-content-center d-flex align-items-center flex-column">
            <Navigation />
          </div>
        </div>
      </div>
    </Router>
  );
}

export default App;
