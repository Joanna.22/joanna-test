import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Redirect, Switch } from 'react-router-dom';
import { getStore } from '../utils';
import Login from '../components/forms/Login';
import Home from '../components/Home';
import Details from '../components/Details';

function AuthenticatedRoute ({component: Component, ...rest}) {
  return (
    <Route
      {...rest}
      render={(props) => getStore('username') ? <Component {...props} />
        : <Redirect to={{pathname: '/', state: {from: props.location}}} />}
    />
  )
}

class Navigation extends Component {
  render() {
    return (
        <Router>
            <Switch>
              <Route path="/" exact={true}>
                <Login />
              </Route>
              <AuthenticatedRoute path="/home" exact={true} component={Home} />
              <AuthenticatedRoute path="/details/:countryID" exact={true} component={Details} />
            </Switch>
        </Router>
    )
  }
}

export default Navigation;